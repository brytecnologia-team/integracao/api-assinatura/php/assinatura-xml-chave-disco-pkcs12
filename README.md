# Geração de Assinatura XML

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia PHP.

Este exemplo apresenta os passos necessários para a geração de assinatura XML utilizando-se chave privada armazenada em disco.
  - Passo 1: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 2: Cifragem dos dados inicializados com o certificado armazenado em disco.
  - Passo 2: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [CURL] - Client URL Library.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| caminhoDoArquivoParaAssinar | Localização do arquivo a ser assinado. | AssinadorXML.php
| token | Access Token para o consumo do serviço (JWT). | AssinadorXML.php
| caminhoDoCertificado | Localização do arquivo da chave privada a ser configurada na aplicação. | AssinadorXML.php
| senhaDoCertificado | Senha do arquivo da chave privada a ser configurada na aplicação. | AssinadorXML.php


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo é necessário ter o PHP 7.x instalado na sua máquina, além de instalar a biblioteca php-curl.

Comandos:

Instalar php-curl (linux):

    -apt-get install php-curl

Caso o seu sistema operacional seja outro, verifique sobre a instalação [aqui](https://www.php.net/manual/pt_BR/curl.requirements.php).

Executar programa:

    -php AssinadorXML.php


   [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
   [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>

